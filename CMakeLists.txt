cmake_minimum_required(VERSION 3.28)
project(cuda_searcher LANGUAGES CXX HIP)
find_package(Threads REQUIRED)

set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

# Add subdirectories
add_subdirectory(lib_cuda_searcher)
add_subdirectory(cuda_searcher)
