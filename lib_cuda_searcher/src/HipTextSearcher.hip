#include <HipTextSearcher.hpp>
#include <Needle.hpp>
#include <SmartDevicePointer.hpp>
#include <Utils.hip>
#include <hip/hip_api.h>
#include <hip/hip_device_launch_parameters.h>
#include <hip/hip_runtime.h>

__global__ void searchKernel(const char *haystack, const char *needle,
                             const int *prefixFunction, char *foundPositions,
                             const int needleLength, const int haystackLength) {
  const int chunkIndex = threadIdx.x + blockIdx.x * blockDim.x;
  const int searchBegin = chunkIndex * needleLength;
  const int searchEnd =
      min((chunkIndex + 2) * needleLength - 1, haystackLength);
  int prefixPosition = 0;
  for (int searchIndex = searchBegin; searchIndex < searchEnd; searchIndex++) {
    while (prefixPosition > 0 &&
           haystack[searchIndex] != needle[prefixPosition]) {
      prefixPosition = prefixFunction[prefixPosition - 1];
    }
    if (haystack[searchIndex] == needle[prefixPosition]) {
      prefixPosition++;
    }
    if (prefixPosition == needleLength) {
      foundPositions[searchIndex + 1 - needleLength] = 1;
      prefixPosition = prefixFunction[prefixPosition - 1];
    }
  }
}

void HipTextSearcher::setAppropriateHaystackChunkSize() {
  throwIfUnsuccessfull(hipOccupancyMaxPotentialBlockSize(&kernelGridSize,
                                                         &kernelBlockSize,
                                                         searchKernel, 0, 0),
                       "Unable to get max occupancy block size.");
  appropriateHaystackChunkSize = (kernelGridSize * kernelBlockSize) * needle.needle.length();
}

HipTextSearcher::HipTextSearcher(const Needle needle)
    : needle(needle), needleDevPtr(needle.needle.length()),
      prefixFunctionDevPtr(needle.needlePrefixFunction.size()) {
  setAppropriateHaystackChunkSize();
}

HipTextSearcher::~HipTextSearcher() {}

std::vector<int> HipTextSearcher::performSearch(std::istream &haystack) {
  std::cout << "im in perform search" << std::endl;
  SmartDevicePointer<char> haystackDevPtr(appropriateHaystackChunkSize);
  SmartDevicePointer<char> foundPositionsDevPtr(appropriateHaystackChunkSize);
  throwIfUnsuccessfull(hipMemcpyHtoD(needleDevPtr.get(), needle.needle.data(),
                                     needleDevPtr.memorySize()),
                       "Unable to copy needle to Device");
  throwIfUnsuccessfull(hipMemcpyHtoD(prefixFunctionDevPtr.get(),
                                     needle.needlePrefixFunction.data(),
                                     prefixFunctionDevPtr.memorySize()),
                       "Unable to copy prefix function to Device.");
  std::unique_ptr<char[]> foundPositions =
      std::make_unique<char[]>(appropriateHaystackChunkSize);
  std::vector<int> results;
  auto buffer = std::make_unique<char[]>(appropriateHaystackChunkSize);
  size_t totalReadChars = 0;
  haystack.read(buffer.get(), needle.needle.length());
  while (haystack.good()) {
    haystack.read(buffer.get() + needle.needle.length(),
                  appropriateHaystackChunkSize - needle.needle.length());
    int readChars = haystack.gcount();
    throwIfUnsuccessfull(hipMemset(foundPositionsDevPtr.get(), 0,
                                   foundPositionsDevPtr.memorySize()),
                         "Unable to set memory");
    throwIfUnsuccessfull(hipMemcpyHtoD(haystackDevPtr.get(), buffer.get(),
                                       readChars + needle.needle.length()),
                         "Unable to copy haystack to Device.");
    hipLaunchKernelGGL(searchKernel, dim3(kernelGridSize), dim3(kernelBlockSize), 0, 0,
                       haystackDevPtr.get(), needleDevPtr.get(),
                       prefixFunctionDevPtr.get(), foundPositionsDevPtr.get(),
                       needle.needle.length(),
                       readChars + needle.needle.length());
    hipDeviceSynchronize();
    throwIfUnsuccessfull(hipMemcpyDtoH(foundPositions.get(),
                                       foundPositionsDevPtr.get(),
                                       foundPositionsDevPtr.memorySize()),
                         "Unable to copy found positions back from Device.");
    memcpy(buffer.get(),
           buffer.get() + appropriateHaystackChunkSize - needle.needle.length(),
           needle.needle.length());
    for (size_t position = 0; position < readChars; position++) {
      if (foundPositions[position] == 1) {
        results.push_back(position + totalReadChars);
      }
    }
    totalReadChars += readChars;
  }
  return results;
}
