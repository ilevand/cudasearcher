#include <HipTextSearcher.hpp>
#include <SearchPerformer.hpp>
#include <StringOrFilepath.hpp>
#include <TextSearcher.hpp>
#include <fstream>
#include <istream>
#include <memory>
#include <sstream>
#include <stdexcept>

SearchPerformer::SearchPerformer(StringOrFilepath haystack,
                                 StringOrFilepath needle,
                                 SearchStrategy strategy)
    : haystack(haystack), needle(needle), strategy(strategy) {}
Needle SearchPerformer::prepareNeedle() {
  switch (this->needle.Type()) {
  case None: {
    throw new std::invalid_argument("Unable to prepare needle as it is empty.");
    break;
  }
  case String: {
    return Needle(needle.GetString());
    break;
  }
  case Filepath: {
    std::ifstream needleFile(needle.GetFilepath());
    if (!needleFile.is_open()) {
      throw new std::runtime_error("Unable to open needle file.");
    }
    std::stringstream buffer;
    buffer << needleFile.rdbuf();
    return Needle(buffer.str());
    break;
  }
    default: {
      throw std::runtime_error("Unable to prepare needle due to its unknown type.");
    }
  }
}
std::unique_ptr<std::istream> SearchPerformer::prepareHaystack() {
  switch (this->haystack.Type()) {
  case None: {
    throw new std::invalid_argument(
        "Unable to prepare haystack as it is empty.");
  }
  case String: {
    return std::unique_ptr<std::istream>(new std::istringstream(this->haystack.GetString()));
  }
  case Filepath: {
    std::ifstream *haystackFile = new std::ifstream(this->haystack.GetFilepath());
    if (!haystackFile->is_open()) {
      throw new std::runtime_error("Unable to open haystack file.");
    }
    return std::unique_ptr<std::istream>(haystackFile);
  }
    default: {
      throw std::runtime_error("Unable to prepare haystack due to its unknown type.");
    }
  }
}

std::vector<int> SearchPerformer::performSearch() {
  auto preparedNeedle = prepareNeedle();
  auto preparedHaystack = prepareHaystack();
  if (strategy == Hip) {
    return HipTextSearcher(preparedNeedle).performSearch(*preparedHaystack);
  }
  return TextSearcher::performSearch(preparedNeedle, *preparedHaystack, strategy);
}
