#include <Needle.hpp>

std::vector<int> Needle::getPrefixFunction(const std::string &s) {
  std::vector<int> result = std::vector<int>(s.length());
  for (int i = 1; i < s.length(); ++i) {
    int prefixPosition = result[i - 1];
    while (prefixPosition > 0 and s[i] != s[prefixPosition]) {
      prefixPosition = result[prefixPosition - 1];
    }
    if (s[i] == s[prefixPosition]) {
      prefixPosition++;
    }
    result[i] = prefixPosition;
  }
  return result;
}

Needle::Needle(const std::string &needle) {
  this->needle_ = needle;
  this->needlePrefixFunction_ = getPrefixFunction(this->needle);
}

Needle::Needle(const Needle &other) {
  this->needle_ = other.needle_;
  this->needlePrefixFunction_ = other.needlePrefixFunction_;
}
