#include <TextSearcher.hpp>
#include <cstdio>
#include <future>
#include <string>
#include <vector>
#include <sstream>

std::vector<int>
TextSearcher::processSearchAll(const std::vector<int> &needlePrefixFunction,
                               const std::string &needle,
                               const std::string &haystack) {
  std::vector<int> result;
  int prefixPosition = 0;
  for (int i = 0; i < haystack.length(); i++) {
    while (prefixPosition > 0 && haystack[i] != needle[prefixPosition]) {
      prefixPosition = needlePrefixFunction[prefixPosition - 1];
    }
    if (haystack[i] == needle[prefixPosition]) {
      prefixPosition++;
    }
    if (prefixPosition == needle.length()) {
      result.push_back(i - needle.length() + 1);
      prefixPosition = needlePrefixFunction[prefixPosition - 1];
    }
  }
  return result;
}

int TextSearcher::processSingleChunk(const Needle &needle,
                                     const std::string &haystack,
                                     std::vector<char> &results,
                                     size_t chunkIndex) {
  int prefixPosition = 0;
  int numberOfResults = 0;
  int begin = needle.needle.length() * chunkIndex;
  int end = needle.needle.length() * (chunkIndex + 2) - 1;
  for (int i = begin; i < end && i < haystack.length(); i++) {
    while (prefixPosition > 0 && haystack[i] != needle.needle[prefixPosition]) {
      prefixPosition = needle.needlePrefixFunction[prefixPosition - 1];
    }
    if (haystack[i] == needle.needle[prefixPosition]) {
      prefixPosition++;
    }
    if (prefixPosition == needle.needle.length()) {
      results[i] = 1;
      numberOfResults++;
      prefixPosition = needle.needlePrefixFunction[prefixPosition - 1];
    }
  }
  return numberOfResults;
}

std::vector<int> TextSearcher::Search(const Needle &needle,
                                      const std::string &haystack) {
  return processSearchAll(needle.needlePrefixFunction, needle.needle, haystack);
}

std::vector<int> TextSearcher::SearchInChunks(const Needle &needle,
                                              const std::string &haystack) {
  size_t numberOfIterations = haystack.length() / needle.needle.length();
  std::vector<char> foundPositions(haystack.length());
  std::vector<int> results;
  for (int i = 0; i < numberOfIterations; i++) {
    processSingleChunk(needle, haystack, foundPositions, i);
  }
  for (int i = 0; i < haystack.length(); i++) {
    if (foundPositions[i] != 0) {
      results.push_back(i - (needle.needle.length() - 1));
    }
  }
  return results;
}

std::vector<int>
TextSearcher::SearchInChunksParallel(const Needle &needle,
                                     const std::string &haystack) {
  std::vector<std::future<int>> searchJobs;
  std::vector<char> foundPositions(haystack.length());
  size_t numberOfIterations = haystack.length() / needle.needle.length();
  for (int i = 0; i < numberOfIterations; i++) {
    searchJobs.push_back(std::async([&needle, &haystack, &foundPositions, i] {
      return processSingleChunk(needle, haystack, foundPositions, i);
    }));
  }
  std::vector<int> results;
  for (int i = 0; i < numberOfIterations; i++) {
    auto result = searchJobs[i].get();
  }
  for (int i = 0; i < haystack.length(); i++) {
    if (foundPositions[i] != 0) {
      results.push_back(i - (needle.needle.length() - 1));
    }
  }
  return results;
}

std::vector<int> TextSearcher::performSearch(const Needle &needle,
                                             std::istream &haystack,
                                             SearchStrategy strategy) {
  std::vector<int> results;
  std::function<std::vector<int>(const Needle &, const std::string &)>
      searchMethod{};
  int TEXT_CHUNK_SIZE = 1024 * 1024;
  switch (strategy) {
  case Flat: {
    searchMethod = TextSearcher::Search;
    break;
  }
  case Serial: {
    searchMethod = TextSearcher::SearchInChunks;
    break;
  }
  case Parallel: {
    searchMethod = TextSearcher::SearchInChunksParallel;
    break;
  }
  }
  std::string textChunk;
  textChunk.reserve(TEXT_CHUNK_SIZE);
  while (haystack.good()) {
    textChunk.resize(TEXT_CHUNK_SIZE);
    haystack.read(textChunk.data(), TEXT_CHUNK_SIZE - needle.needle.length());
    int readChars = haystack.gcount();
    textChunk.resize(readChars);
    auto chunkSearchResults = searchMethod(needle, textChunk);
    results.insert(results.end(), chunkSearchResults.begin(),
                   chunkSearchResults.end());
  }
  return results;
}
