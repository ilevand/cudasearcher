#include <StringOrFilepath.hpp>
#include <stdexcept>
#include <string>

StringOrFilepathType StringOrFilepath::Type() { return this->type; }
std::string StringOrFilepath::GetString() {
  if (this->type != String) {
    throw new std::runtime_error(
        "Data vioation. Try of getting String while it is not.");
  }
  return this->data;
}
std::string StringOrFilepath::GetFilepath() {
  if (this->type != Filepath) {
    throw new std::runtime_error(
        "Data vioation. Try of getting Filepath while it is not.");
  }
  return this->data;
}
StringOrFilepath StringOrFilepath::NewFilepath(const char *filepath) {
  StringOrFilepath sop;
  sop.type = Filepath;
  sop.data = std::string(filepath);
  return sop;
}
StringOrFilepath StringOrFilepath::NewString(const char *string) {
  StringOrFilepath sop;
  sop.type = String;
  sop.data = std::string(string);
  return sop;
}
