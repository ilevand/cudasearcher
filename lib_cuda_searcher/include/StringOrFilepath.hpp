#include <string>
#include <stdexcept>

#pragma once

enum StringOrFilepathType { None, String, Filepath };

class StringOrFilepath {
private:
  std::string data;
  StringOrFilepathType type;
public:
  StringOrFilepath() : type(None) {}
  StringOrFilepathType Type();
  std::string GetString();
  std::string GetFilepath();
  static StringOrFilepath NewFilepath(const char *filepath);
  static StringOrFilepath NewString(const char *string);
};
