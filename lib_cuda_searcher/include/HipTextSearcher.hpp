#pragma once
#include <SmartDevicePointer.hpp>
#include <Needle.hpp>
#include <string>
#include <vector>

class HipTextSearcher {
  uint kernelGridSize;
  uint kernelBlockSize;
  int appropriateHaystackChunkSize;
  Needle needle;
  SmartDevicePointer<char> needleDevPtr;
  SmartDevicePointer<int> prefixFunctionDevPtr;
  std::vector<int> launchSeacrchKernel(const Needle &needle,
                                       const std::string haystack);
  void setAppropriateHaystackChunkSize();
public:
  HipTextSearcher(Needle needle);
  ~HipTextSearcher();
  std::vector<int> performSearch(std::istream &haystack);
};
