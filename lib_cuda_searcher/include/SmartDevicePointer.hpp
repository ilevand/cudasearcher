#pragma once
#include <cstddef>

template<typename T> class SmartDevicePointer {
private:
  void* pointer_;
  size_t size_;
public:
  T* get();
  size_t memorySize();
  SmartDevicePointer();
  SmartDevicePointer(size_t size);
  ~SmartDevicePointer();
};
