#pragma once

enum SearchStrategy {
  Flat,
  Serial,
  Parallel,
  Hip
};
