#pragma once
#include <Needle.hpp>
#include <SearchStrategy.hpp>
#include <StringOrFilepath.hpp>
#include <TextSearcher.hpp>
#include <istream>
#include <memory>
#include <vector>

class SearchPerformer {
private:
  StringOrFilepath haystack;
  StringOrFilepath needle;
  SearchStrategy strategy;

  Needle prepareNeedle();
  std::unique_ptr<std::istream> prepareHaystack();
public:
  SearchPerformer();
  SearchPerformer(StringOrFilepath haystack, StringOrFilepath needle, SearchStrategy strategy = Flat);
  std::vector<int> performSearch();
};
