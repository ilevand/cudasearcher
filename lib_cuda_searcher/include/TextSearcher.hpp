#pragma once
#include <SearchStrategy.hpp>
#include <vector>
#include <string>
#include <Needle.hpp>

class TextSearcher {
private:
  static std::vector<int> processSearchAll(const std::vector<int> &needlePrefixFunction, const std::string &needle, const std::string &haystack);
  static int processSingleChunk(const Needle &needle, const std::string &haystack, std::vector<char> &results, size_t index);
  static std::vector<int> Search(const Needle &needle, const std::string &haystack);
  static std::vector<int> SearchInChunks(const Needle &needle, const std::string &haystack);
  static std::vector<int> SearchInChunksParallel(const Needle &needle, const std::string &haystack);
  static std::vector<int> SearchInChunksCuda(const Needle &needle, const std::string &haystack);
public:
  static std::vector<int> performSearch(const Needle &needle, std::istream &haystack, SearchStrategy strategy);
};
