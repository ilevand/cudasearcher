#pragma once
#include <string>
#include <vector>

class Needle {
private:
  std::string needle_;
  std::vector<int> needlePrefixFunction_;
  static std::vector<int> getPrefixFunction(const std::string &s);
public:
  const std::string &needle = needle_;
  const std::vector<int> &needlePrefixFunction = needlePrefixFunction_;
  Needle(const std::string &s);
  Needle(const Needle& other);
};
