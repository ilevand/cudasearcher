#include <SearchStrategy.hpp>
#include <SearchPerformer.hpp>
#include <StringOrFilepath.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int main(int argc, char *argv[]) {
  StringOrFilepath haystack;
  StringOrFilepath needle;
  SearchStrategy strategy = Flat;
  for (int i = 1; i < argc; i++) {
    std::string arg1(argv[i]);
    if (arg1 == "--haystack") {
      if (i + 1 >= argc) {
        std::printf("Error parsing haystack.\n");
        std::printf("Check --help for examples.\n");
        return 0;
      }
      std::string arg2(argv[i + 1]);
      if (arg2 == "--file" || arg2 == "-f") {
        if (i + 2 >= argc) {
          std::printf("Error parsing haystack.\n");
          std::printf("Check --help for examples.\n");
          return 0;
        }
        haystack = StringOrFilepath::NewFilepath(argv[i + 2]);
        i += 2;
      } else {
        haystack = StringOrFilepath::NewString(arg2.c_str());
        i++;
      }
      continue;
    }

    if (arg1 == "--needle") {
      if (i + 1 >= argc) {
        std::printf("Error parsing needle.\n");
        std::printf("Check --help for examples.\n");
        return 0;
      }
      std::string arg2(argv[i + 1]);
      if (arg2 == "--file" || arg2 == "-f") {
        if (i + 2 >= argc) {
          std::printf("Error parsing needle.\n");
          std::printf("Check --help for examples.\n");
          return 0;
        }
        needle = StringOrFilepath::NewFilepath(argv[i + 2]);
        i += 2;
      } else {
        needle = StringOrFilepath::NewString(arg2.c_str());
        i++;
      }
      continue;
    }
    if (arg1 == "--serial") {
      strategy = Serial;
    }
    if (arg1 == "--parallel") {
      strategy = Parallel;
    }
    if (arg1 == "--hip") {
      strategy = Hip;
    }
  }
  if (needle.Type() == None || haystack.Type() == None) {
    std::printf("Error: wrong usage\n");
    std::printf("Check --help for examples\n");
    return 1;
  }
  SearchPerformer performer(haystack, needle, strategy);
  std::vector<int> results = performer.performSearch();
  std::stringstream output;
  for (int index : results) {
    output << index << " ";
  }
  std::printf("%s\n", output.str().c_str());
  return 0;
}
